function State() { }
State.prototype.onEntry = function () { this._in = true; this.log("onEntry"); this._in = false };
State.prototype.onExit = function () { this._in = true; this.log("onExit"); this._in = false };
State.prototype.onUnknownEvent = function (ev) { throw new Error("Unhandled event: " + evtToStr(ev.type, ev.payload))};

function Actor(St, tracing = false) {
    ensure(St, "Invalid initial state: " + St);
    ensure(St.prototype instanceof State, "InitialState must inherit from 'State'");
    this._tracing = tracing;
    this._state = St;
    this._in = true;
    this._state.prototype["onEntry"].bind(this)();
    this._in = false;
    this.log("initial state: " + JSON.stringify(this));
}

Actor.prototype.name = "Actor";
Actor.prototype.transition = function (state) { this.log("transition(" + state.name + ")"); this._txTo = state };
Actor.prototype.log = function (text) {
    if (this._tracing) console.log(this.name + "[" + this._state.name + "]" + (this._in ? "$ " : "# ") + text)
};
Actor.prototype.send = function (evType, evData) {
    setTimeout(() => dispatchEventToActor(this, evType, evData));
    this.log("send(" + evtToStr(evType, evData) + ")");
};

function ensure(condition, msg) { if (!condition) throw new Error(msg) }
function evtToStr(evType, evData) { return evType + (!evData ? "{}" : JSON.stringify(evData))}
function dispatchEventToActor(actor, evType, evData) {
    actor.log(">> " + evtToStr(evType, evData));
    let evtHandler = actor._state.prototype[evType];
    if (!evtHandler) { actor.send("onUnknownEvent", {evtType: evType, payload: evData}); } else {
        actor._in = true;
        evtHandler.bind(actor)(evData);
        actor._in = false;
        if (actor._txTo) {
            actor._in = true;
            actor._state.prototype["onExit"].bind(actor)();
            actor._in = false;
            actor._state = actor._txTo;
            actor._in = true;
            actor._state.prototype["onEntry"].bind(actor)();
            actor._in = false;
            actor.log("Transition done");
            actor._txTo = 0;
        }
    }
    actor.log("<<")
}

exports.State = State;
exports.Actor = Actor;


