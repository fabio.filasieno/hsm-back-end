
let db = require("./database.js");
let utils = require("./utils");


// ------------ STATE REQUEST -----------
//CREATE NEW PROPOSAL
exports.createNewProposal = function (req, actorManager){
    let propId = req.body.id;
    actorManager.send('newProposal', {id: propId});
}

//VALIDATE PRODUCT DATA (STEP #1 PRODOTTO)
exports.saveProductState = async  function (req, actorManager) {
    let propId = req.body.id;
    let product = req.body.product;
    let sql = "select * from summary_state where id = ?";
    let params = [propId];
    let row = await utils.getSql(sql, params);
    if(row && row.id) actorManager.send('checkProductActorMap', {summary_state: row, product: product});
    // La proposta non esiste
    else actorManager.log("ERROR! ID proposal not found");
}

//VALIDATE CUSTOMER DATA (STEP #2 ANAGRAFICA)
exports.saveCustomerData = function (req, actorManager) {
    console.log(req.body);
    let propId = req.body.id;
    let customer = req.body.customer;
    let sql = "select * from summary_state where id = ?";
    let params = [propId];
    db.get(sql, params, (err, row) => {
        if (err) {
            console.log(err.message);
            return;
        }
        if(row && row.id)  actorManager.send('checkActorMap', {summary_state: row, customer: customer});
        // La proposta non esiste
        else actorManager.log("ERROR! ID proposal does not exist");
    })

}

//VALIDATE PAYMENT DATA (STEP #3 PAGAMENTO)
exports.savePaymentData = function (req, actorManager) {
    console.log(req.body);
    let propId = req.body.id;
    let payment = req.body.payment;
    let sql = "select * from summary_state where id = ?"
    let params = [propId];
    db.get(sql, params, (err, row) => {
        if (err) {
            console.log(err.message);
            return;
        }
        if(row && row.id)  actorManager.send( 'checkActorMap', {summary_state: row, propId, payment: payment});
        // La proposta non esiste
        else actorManager.log("ERROR! ID proposal does not exist");
    })
}

// --------- SUMMARY_STATE ------------
//GET ALL STATES
exports.getSummarySate = function () {
    let sql = "select * from summary_state"
    let params = []
    db.all(sql, params, (err, rows) => {
        if (err) return res.status(400).json({"error":err.message});
        return res.json({
            "message":"success",
            "data":rows
        })
    });
}
//GET A SINGLE STATE
exports.getSingleState = function (req, res) {
    let sql = "select * from summary_state where id = ?"
    let params = [req.params.id]
    db.get(sql, params, (err, row) => {
        if (err) return res.status(400).json({"error":err.message});
        return res.json({
            "message":"success",
            "data":row
        })
    });
}

exports.createNewState = function (req, res) {
    console.log(req.body);
    let data = {
        id: req.body.summary_id
    }
    console.log(data);
    let sql ="INSERT INTO summary_state (id, customer_data, product, payment) VALUES (?,'TODO', 'TODO', 'TODO')"
    let params =[data.id];
    db.run(sql, params, function (err, result) {
        if (err) return  res.status(400).json({"error": err.message})
        return res.json({
            "message": "success",
            "data": data,
            "id" : this.lastID
        })
    });
}

exports.updateState = function (req, res) {
    let data = {
        customer_data: req.body.customer_data,
        product: req.body.product,
        payment : req.body.payment
    }
    db.run(
        `UPDATE summary_state set 
           customer_data = COALESCE(?,customer_data), 
           product = COALESCE(?,product), 
           payment = COALESCE(?,payment) 
           WHERE id = ?`,
        [data.customer_data, data.product, data.payment, req.params.id],
        function (err, result) {
            if (err) return res.status(400).json({"error": res.message});
            return res.json({
                message: "success",
                data: data,
                changes: this.changes//Num of updates rows
            })
        });
}

exports.deleteState = function (req, res){
    db.run(
        'DELETE FROM summary_state WHERE id = ?',
        req.params.id,
        function (err, result) {
            if (err) return res.status(400).json({"error": res.message});
            return res.json({"message":"deleted", changes: this.changes});
        });
}

/* --------- CUSTOMER_DATA ------------
//GET A SINGLE CUSTOMER
app.get("/api/customer/:id", (req, res, next) => {
    let sql = "select * from customer_data where customer_id = ?"
    let params = [req.params.id]
    db.get(sql, params, (err, row) => {
        if (err) {
            res.status(400).json({"error":err.message});
            return;
        }
        res.json({
            "message":"success",
            "data":row
        })
    });
});
//CREATE CUSTOMER
app.post("/api/customer/", (req, res, next) => {
    let data = {
        customer_id: req.body.id,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        street : req.body.street,
        city: req.body.city,
        state: req.body.state,
        zip: req.body.zip
    }
    let sql ='INSERT INTO customer_data (customer_id,firstname, lastname, email, street, city, state, zip) VALUES (?,?,?,?,?,?,?,?)'
    let params =[data.customer_id, data.firstname, data.lastname, data.email, data.street, data.city, data.state, data.zip]
    db.run(sql, params, function (err, result) {
        if (err){
            res.status(400).json({"error": err.message})
            return;
        }
        res.json({
            "message": "success",
            "data": data,
            "id" : this.lastID
        })
    });
})
//UPDATE CUSTOMER
app.patch("/api/customer/:id", (req, res, next) => {
    let data = {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        street : req.body.street,
        city: req.body.city,
        state: req.body.state,
        zip: req.body.zip
    }
    db.run(
        `UPDATE customer_data set
           firstname = COALESCE(?, firstname),
           lastname = COALESCE(?, lastname),
           email = COALESCE(?, email),
           street = COALESCE(?, street),
           city = COALESCE(?, city),
           state = COALESCE(?, state),
           zip = COALESCE(?, zip)
           WHERE customer_id = ?`,
        [data.firstname, data.lastname, data.email, data.street, data.city, data.state, data.zip, req.params.id],
        function (err, result) {
            if (err){
                res.json({
                    message: err.message,
                })
                res.status(400).json({"error": res.message})
                return;
            }
            res.json({
                message: "success",
                data: data,
                changes: this.changes
            })
        });
});
//DELETE CUSTOMER
app.delete("/api/customer/:id", (req, res, next) => {
    db.run(
        'DELETE FROM customer_data WHERE customer_id = ?',
        req.params.id,
        function (err, result) {
            if (err){
                res.status(400).json({"error": res.message})
                return;
            }
            res.json({"message":"deleted", changes: this.changes})
        });
})

// --------- PRODUCT ------------
//GET A SINGLE PRODUCT
app.get("/api/product/:id", (req, res, next) => {
    let sql = "select * from product where product_id = ?"
    let params = [req.params.id]
    db.get(sql, params, (err, row) => {
        if (err) {
            res.status(400).json({"error":err.message});
            return;
        }
        res.json({
            "message":"success",
            "data":row
        })
    });
});
//CREATE PRODUCT
app.post("/api/product/", (req, res, next) => {
    let data = {
        product_id: req.body.id,
        productName : req.body.productName,
        award: req.body.award,
        package: req.body.package,
        frequency: req.body.frequency
    }
    let sql ="INSERT INTO product (product_id, productName, award, package, frequency) VALUES (?,?,?,?,?)"
    let params =[data.product_id, data.productName, data.award, data.package, data.frequency]
    db.run(sql, params, function (err, result) {
        if (err){
            res.status(400).json({"error": err.message})
            return;
        }
        res.json({
            "message": "success",
            "data": data,
            "id" : this.lastID
        })
    });
})

//UPDATE PRODUCT
app.patch("/api/product/:id", (req, res, next) => {
    let data = {
        productName : req.body.productName,
        award: req.body.award,
        package: req.body.package,
        frequency: req.body.frequency
    }
    db.run(
        `UPDATE product set
           productName = COALESCE(?, productName),
           award = COALESCE(?, award),
           package = COALESCE(?, package),
           frequency = COALESCE(?, frequency)
           WHERE product_id = ?`,
        [data.productName, data.award, data.package, data.frequency, req.params.id],
        function (err, result) {
            if (err){
                res.json({
                    message: err.message,
                })
                res.status(400).json({"error": res.message})
                return;
            }
            res.json({
                message: "success",
                data: data,
                changes: this.changes
            })
        });
});
//DELETE PRODUCT
app.delete("/api/product/:id", (req, res, next) => {
    db.run(
        'DELETE FROM product WHERE product_id = ?',
        req.params.id,
        function (err, result) {
            if (err){
                res.status(400).json({"error": res.message})
                return;
            }
            res.json({"message":"deleted", changes: this.changes})
        });
})
*/