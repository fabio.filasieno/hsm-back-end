var sqlite3 = require('sqlite3').verbose()

const DBSOURCE = "db.sqlite"

let db = new sqlite3.Database(DBSOURCE, (err) => {
    if (err) {
        // Cannot open database
        console.error(err.message)
        throw err
    }else{
        console.log('Connected to the SQLite database.')
        db.run(`
          CREATE TABLE product (
                product_id text PRIMARY KEY NOT NULL,
                productName text,
                award float,
                package text,
                frequency text,
                CONSTRAINT fk FOREIGN KEY (product_id)  REFERENCES summary_state (id)  ON DELETE CASCADE
             );`,
            (err) => {
                if (err) console.log(err.message);
            });
        db.run(`
           CREATE TABLE customer_data (
                customer_id text PRIMARY KEY NOT NULL,
                firstname text,
                lastname text,
                email text UNIQUE, 
                street text,
                city text, 
                state text,
                zip int,
                CONSTRAINT fk FOREIGN KEY (customer_id)  REFERENCES summary_state (id)  ON DELETE CASCADE
            ); `,
            (err) => {
                if (err) console.log(err.message);
            });
        db.run(`
           CREATE TABLE payment (
                payment_id text PRIMARY KEY NOT NULL,
                type string,
                amount float,
                payerName text,
                payerLastName text,
                 CONSTRAINT fk FOREIGN KEY (payment_id)  REFERENCES summary_state (id)  ON DELETE CASCADE
            ); `,
            (err) => {
                if (err) console.log(err.message);
            });
        db.run(`
           CREATE TABLE summary_state (
                id text PRIMARY KEY NOT NULL ,
                product_state text,
                customer_state text, 
                payment_state text
            ); 
            `,
            (err) => {
                if (err) console.log(err.message);
            });
        //
        db.run(` PRAGMA foreign_keys=on; `,
            (err) => {
                if (err) console.log(err.message);
            });
    }
});


module.exports = db;