const actorManagerState = require("./actorManager");
const fsm = require("./fsm");

describe("Main Actor", function() {
    var mainActor;
    it("Creates the main actor", function() {
        mainActor = new fsm.Actor(actorManagerState.ActorManagerInitState, true);
        expect(mainActor).toEqual(jasmine.any(fsm.Actor));
    });
    it("Main Actor contains _state properties ", function () {
        expect(mainActor).toEqual(jasmine.objectContaining({
            _state: ActorManagerInitState
        }))
    })
});

beforeAll(function() {
    console.log("Delete the database file");
   /* const fs = require('fs');
    const path = './db.sqlite';
    try {
        fs.unlinkSync(path)
        //file removed
    } catch(err) {
        console.error(err)
    }
  */
});