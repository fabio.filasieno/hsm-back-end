const fsm = require("./fsm");
let utils = require("./utils");
let ProductState = require("./productState");

let actorProductMap = new Map();

async function newProposal(data) {
    console.log("newProposal id " + data.id);
    let customer_data_state = data.customer_state || 'NEW';
    let product_state = data.product_state || 'NEW';
    let payment_state = data.payment_state || 'NEW';
    let sql = "INSERT INTO summary_state (id, customer_state, product_state, payment_state) VALUES (?,?,?,?)";
    let params = [data.id, customer_data_state, product_state, payment_state];
    let result = await utils.execSql(sql, params);
    return result;
}

 class ActorManagerInitState extends fsm.State{

    async newProposal(data) {
        let result = await newProposal(data);
        if (typeof result === "string") {
            this.log(result);
            return;
        }
        if (result === true){
            this.log("Proposal created!");
            let actor = new fsm.Actor(ProductState.WaitingProduct, true);
            actor.name = "ProductStateActor";
            actorProductMap.set(data.id, actor);
        }
    }

    async checkProductActorMap(evt){
        let actor;
        actor =  actorProductMap.get(evt.summary_state.id);
        if(actor){
            if(actor._state.name === "WaitingProduct") actor.send("checkProduct", evt);
            else if(actor._state.name ==="ProductDone") actor.send("productUpdate", evt);
            else this.log("Invalid sate");
        }
        else{
            this.log("ERROR - Actor doesn't exist for proposal "+evt.summary_state.id);
        }
    }
}

exports.ActorManagerInitState = ActorManagerInitState;


/*
    async checkMapAnagrafica(evt){//STEP #2
        let sql = "select * from summary_state where id = ?"
        let params = [evt.id];
        let row = await utils.getSql(sql, params);
        evt.customer_data = row.customer_data;
        if(row && row.product!=="DONE"){
            this.log("Proposal "+evt.id+": has PRODUCT state set to "+row.product+", sending back to PRODUCT state");
            this.send("checkMapProdotto", evt);
            return ;
        }
        if(row && row.product==="DONE" && row.customer_data==="DONE") this.log("Proposal "+evt.id+": CUSTOMER_DATA UPDATE");
        let actor;
        actor =  customerMap.get(evt.id);
        if(actor){
            actor.state = ProductStateNew;
            actor.send('checkCustomer', evt);
        }
        else{
            this.log('Creating new Actor of CustomerStateNew');
            actor = new fsm.Actor(CustomerStateNew, true);
            actor.name = "CustomerActor";
            customerMap.set(evt.id, actor);
            actor.send('checkCustomer', evt);
        }
    }

    async checkMapPayment(evt){
        let sql = "select * from summary_state where id = ?"
        let params = [evt.id];
        let row = await utils.getSql(sql, params);
        evt.payment = row.payment;
        if(row && row.customer_data!=="DONE"){
            this.log("Proposal "+evt.id+": has CUSTOMER_DATA state set to "+row.customer_data+", sending back to CUSTOMER_DATA state");
            this.send("checkMapAnagrafica", evt);
            return ;
        }
        let actor;
        actor =  paymentMap.get(evt.id);
        if(actor){
            actor.state = PaymentStateNew;
            actor.send('checkPayment', evt);
        }
        else{
            this.log('Creating new Actor of PaymentStateNew');
            actor = new fsm.Actor(PaymentStateNew, true);
            actor.name = "Payment";
            paymentMap.set(evt.id, actor);
            actor.send('checkPayment', evt);
        }
    }
  */