const fsm = require("./fsm");
let db = require("./database.js");

let productMap = new Map();

class ProductStateNew extends fsm.State{
    onEntry(){
        this.log('ProductStateNew')
    }

   async checkProduct(evt){
        this.id = evt.id;
        if(evt.product){
            let error = false;
            if(!evt.product.productName){
                this.log("Field productName is null!");
                error = true;
            }
            if(!evt.product.award){
                this.log("Field award is null!");
                error = true;
            }
            if(!evt.product.package){
                this.log("Field package is null!");
                error = true;
            }
            if(!evt.product.frequency){
                this.log("Field frequency is null!");
                error = true;
            }
            if(error===true){
                this.transition(ProductStateError);
                this.state = ProductStateError;
                return;
            }
            let data = {
                product_id: evt.id,
                productName : evt.product.productName,
                award: evt.product.award,
                package: evt.product.package,
                frequency: evt.product.frequency
            }
            let sql, params;
            if(evt.product!=="DONE") {
                sql ="INSERT INTO product (product_id, productName, award, package, frequency) VALUES (?,?,?,?,?)";
                params =[data.product_id, data.productName, data.award, data.package, data.frequency];
            }
            else {
                sql = `UPDATE product SET
                       productName = productName,
                       award =  award,
                       package = package,
                       frequency = frequency
                       WHERE id = ?`;
                params = [data.productName, data.award, data.package, data.frequency, data.product_id];
            }
            let result = await execSql (sql, params);
            if(typeof result==="string"){
                this.log(result);
                this.transition(ProductStateError);
                this.state = ProductStateError;
            }
            else if (result===true){
                console.log('Product id '+evt.id+' success');
                sql= `UPDATE summary_state set product = 'DONE' WHERE id = ?`;
                params = [evt.id];
                let update = await execSql(sql, params);
                if(update===true)  this.log('Product state set to DONE!');
                else if(typeof update === "string") this.log(update);
            }
        } else {
            this.log("No PRODUCT data received!");
            this.transition(ProductStateError);
            this.state = ProductStateError;
        }
    }
}

class ProductStateError extends fsm.State{
    onEntry(){
        productMap.set(this.id, this);
        this.log('ProductStateError');
        this.send('setProductError');
    }
    async setProductError(){
        this.log('setProductError for proposal '+this.id);
        let sql =  `UPDATE summary_state set product = 'ERROR' WHERE id = ?`;
        let params =  [this.id];
        let result = await execSql(sql, params);
        if(result===true) this.log('PRODUCT state set to ERROR');
        else if(typeof result==="string") this.log(result);
    }
}


class CustomerStateNew extends fsm.State{
    onEntry(){
        this.log('CustomerStateNew')
    }
    async checkCustomer(evt){
        this.id = evt.id;
        if(evt.anagrafica){
            let error = false;
            if(!evt.anagrafica.firstname){
                this.log("Field firstname is null!");
                error = true;
            }
            if(!evt.anagrafica.lastname){
                this.log("Field lastname is null!");
                error = true;
            }
            if(!evt.anagrafica.email){
                this.log("Field email is null!");
                error = true;
            }
            if(!evt.anagrafica.street){
                this.log("Field street is null!");
                error = true;
            }
            if(!evt.anagrafica.state){
                this.log("Field state is null!");
                error = true;
            }
            if(!evt.anagrafica.zip){
                this.log("Field zip is null!");
                error = true;
            }
            if(error===true){
                this.transition(CustomerStateError);
                this.state = CustomerStateError;
                return;
            }
            let sql, params;
            if(evt.customer_data!=="DONE") {
                sql ="INSERT INTO customer_data (customer_id,firstname, lastname, email, street, city, state, zip) VALUES (?,?,?,?,?,?,?,?)";
                params =[evt.customer_id, evt.firstname, evt.lastname, evt.email, evt.street, evt.city, evt.state, evt.zip];
            }
            else {
                sql = `UPDATE customer_data set 
                       firstname = firstname, 
                       lastname = lastname, 
                       email = email, 
                       street = street,
                       city = city,
                       state = state,
                       zip =  zip
                       WHERE id = ?`;
                params = [evt.firstname, evt.lastname, evt.email, evt.street, evt.city, evt.state, evt.zip, evt.id]
            }
            let result = await execSql (sql, params);
            if(typeof result==="string"){
                this.log(result);
                this.transition(CustomerStateError);
                this.state = CustomerStateError;
            }
            else if (result===true){
                console.log('Customer id '+evt.id+' success');
                sql= `UPDATE summary_state set customer_data = 'DONE' WHERE id = ?`;
                params = [evt.id];
                let update = await execSql(sql, params);
                if(update===true)  this.log('CUSTOMER state set to DONE!');
                else if(typeof update === "string") this.log(update);
            }
        } else {
            this.log("No CUSTOMER data received!")
            this.transition(CustomerStateError);
            this.state = CustomerStateError;
        }
    }
}

class CustomerStateError extends fsm.State{
    onEntry(){
        productMap.set(this.id, this);
        this.log('CustomerStateError');
        this.send('setCustomerError');
    }
    async setCustomerError(){
        this.log('setCustomerError for proposal '+this.id);
        let sql =  `UPDATE summary_state set customer_data = 'ERROR' WHERE id = ?`;
        let params =  [this.id];
        let result = await execSql(sql, params);
        if(result===true) this.log('CUSTOMER_DATA state set to ERROR');
        else if(typeof result==="string") this.log(result);
    }
}

class PaymentStateNew extends fsm.State{
    onEntry(){
        this.log('PaymentStateNew')
    }
    async checkPayment(evt){
        this.id = evt.id;
        if(evt.pagamento){
            let error = false;
            if(!evt.pagamento.type){
                this.log("Field type is null!");
                error = true;
            }
            if(!evt.pagamento.amount){
                this.log("Field amount is null!");
                error = true;
            }
            if(!evt.pagamento.payerName){
                this.log("Field payerName is null!");
                error = true;
            }
            if(!evt.pagamento.payerLastName){
                this.log("Field payerLastName is null!");
                error = true;
            }
            if(error===true){
                this.transition(PaymentStateError);
                this.state = PaymentStateError;
            }
            let sql, params;
            if(evt.payment!=="DONE") {
                sql ="INSERT INTO payment (payment_id, type, amount, payerName, payerLastName) VALUES (?,?,?,?,?)";
                params =[ evt.id, evt.pagamento.type, evt.pagamento.amount, evt.pagamento.payerName, evt.pagamento.payerLastName];
            }
            else {
                sql = `UPDATE payment set 
                       type = type, 
                       amount = amount, 
                       payerName = payerName, 
                       payerLastName = payerLastName
                       WHERE id = ?`;
                params = [ evt.pagamento.type, evt.pagamento.amount, evt.pagamento.payerName, evt.pagamento.payerLastName, evt.id]
            }
            let result = await execSql (sql, params);

        } else {
            this.log("No PAYMENT data received!")
            this.transition(PaymentStateError);
            this.state = PaymentStateError;
        }
    }
}

class PaymentStateError extends fsm.State{
    onEntry(){
        productMap.set(this.id, this);
        this.log('PaymentStateError');
        this.send('setPaymentError');
    }
    async setPaymentError(){
        this.log('setPaymentError for proposal '+this.id);
        let sql =  `UPDATE summary_state set payment = 'ERROR' WHERE id = ?`;
        let params =  [this.id];
        let result = await execSql(sql, params);
        if(result===true) this.log('PAYMENT state set to ERROR');
        else if(typeof result==="string") this.log(result);
    }
}


module.exports = InitialState;