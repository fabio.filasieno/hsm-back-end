
const fsm = require("./fsm");
let utils = require("./utils");

function checkProductFields(evt){
    let error = false;
    if(!evt.product.productName){
        console.log("Field productName is null!");
        error = true;
    }
    if(!evt.product.award){
        console.log("Field award is null!");
        error = true;
    }
    if(!evt.product.package){
        console.log("Field package is null!");
        error = true;
    }
    if(!evt.product.frequency){
        console.log("Field frequency is null!");
        error = true;
    }
    return error;
}

class WaitingProduct extends fsm.State{
    checkProduct(evt){
        if(evt.product){
            let error = checkProductFields(evt);
            if(error===true){
                this.log("Can't go on, current state: "+this.state);
                return;
            }
            this.product = evt.product;
            this.summary_state = evt.summary_state;
            this.transition(ProductReceived);
            this.send("proceedToStore");
        }
        else  this.log("Can't go on, product data not received, current state: "+this.state);
    }
}

class ProductReceived extends fsm.State{
   onEntry(){
       this.log("Received product data for proposal "+this.summary_state.id);
   }
   proceedToStore(){
       this.transition(WaitingDBProductStore);
       this.send("storeProduct");
   }

}

class WaitingDBProductStore extends fsm.State{
    onEntry(){
        this.log("WaitingDBProductStore for proposal "+this.summary_state.id);
    }
     storeProduct(){
        let sql ="INSERT INTO product (product_id, productName, award, package, frequency) VALUES (?,?,?,?,?)";
        let params = [this.summary_state.id, this.product.productName, this.product.award, this.product.package, this.product.frequency];
       // let result =  utils.execSql (sql, params);
        utils.execSql (sql, params).then((success) => {
            this.log("Next state: UpdatingProductState");
            this.transition(UpdatingProductState);
            this.send("setProductStateToDone");
        }).catch((err)=>{
            this.log(err);
            this.transition(WaitingProduct);
        })
    }
}

class UpdatingProductState extends fsm.State{
    onEntry(){
        this.log("UpdatingProductState for proposal "+this.summary_state.id);
    }
    async setProductStateToDone(){
        let sql= `UPDATE summary_state set product_state = 'DONE' WHERE id = ?`;
        let params = [this.summary_state.id];
        let update = await utils.execSql(sql, params);
        if(update===true)  this.log('Product state set to DONE!');
        else{
            this.log(update);//Error
            this.transition(WaitingProduct);
            return;
        }
    }
}

class ProductDone extends fsm.State{
    onEntry(){
        this.log(this.state);
    }
    productUpdate(evt){
      this.transition(WaitingProductUpdate);
      this.send("checkProduct", evt);
    }
}

class WaitingProductUpdate extends fsm.State{
    checkProduct(evt){
        if(evt.product) {
            let err = checkProductFields(evt);
            if(err===true){
                this.log("Can't update product, go back to state ProductDone");
                this.transition(ProductDone);
                this.state = ProductDone;
                return;
            }
            this.product = evt.product;
            this.summary_state = evt.summary_state;
            this.transition(ProductUpdateReceived);
            this.send("proceedToUpdate");
        }
        else{
            this.log("Can't update product, go back to state ProductDone");
            this.transition(ProductDone);
            this.state = ProductDone;
        }
    }
}

class ProductUpdateReceived extends fsm.State{
    onEntry(){
        this.log("Received product data for proposal "+this.summary_state.id);
    }
    proceedToUpdate(){
        this.transition(WaitingDBProductUpdate);
        this.send("updateProduct");
    }
}

class WaitingDBProductUpdate extends fsm.State{

    async updateProduct(){
        let data = {
            product_id: this.summary_state.id,
            productName : this.product.productName,
            award: this.product.award,
            package: this.product.package,
            frequency: this.product.frequency
        }
        let sql = `UPDATE product SET
                   productName = productName,
                   award =  award,
                   package = package,
                   frequency = frequency
                   WHERE id = ?`;
        let params =[data.productName, data.award, data.package, data.frequency, data.product_id];
        let result = await utils.execSql (sql, params);
        if(typeof result==="string"){
            this.log(result);
            this.transition(WaitingProduct);
            this.state = WaitingProduct;
            return;
        }
        if (result === true){
            this.transition(UpdatingProductState);
            this.send("setProductStateToDone");
        }

    }
}

exports.WaitingProduct = WaitingProduct;
exports.ProductDone = ProductDone;