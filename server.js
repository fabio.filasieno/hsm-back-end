// Create express app
let express = require("express");
let api = require("./api.js");
const fsm = require("./fsm");
const actorManagerState = require("./actorManager");


let actorManager;
const app = express();

let bodyParser = require("body-parser");
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// Server port
const HTTP_PORT = 8000;

// Start server
app.listen(HTTP_PORT, () => {
    console.log("Server running on port %PORT%".replace("%PORT%",HTTP_PORT));
    actorManager = new fsm.Actor(actorManagerState.ActorManagerInitState, true);
    actorManager.name = "ActorManager";
});

// Root endpoint
app.get("/", (req, res, next) => {
    res.json({"message":"Ok"})
});

//Other API endpoints
/* ------------ STATE REQUEST ----------- */
//CREATE NEW PROPOSAL
app.post("/api/proposal", (req, res, next) => {
     api.createNewProposal(req, actorManager);
     res.status(200).json({
        "message":"success"
    })
});

//VALIDATE PRODUCT DATA (STEP #1 PRODOTTO)
app.post("/api/product", (req, res, next) => {
    api.saveProductState(req, actorManager);
    res.status(200).json({
        "message":"success"
    })
});

//VALIDATE CUSTOMER DATA (STEP #2 ANAGRAFICA)
app.post("/api/customer", (req, res, next) => {
    api.saveCustomerData(req, actorManager);
     res.status(200).json({
        "message":"success"
    });
});


//VALIDATE PAYMENT DATA (STEP #3 PAGAMENTO)
app.post("/api/payment", (req, res, next) => {
    api.savePaymentData(req, actorManager);
    res.status(200).json({
        "message":"success"
    })
});

// --------- SUMMARY_STATE ------------
//GET ALL STATES
app.get("/api/summary", (req, res, next) => {
    api.getSummarySate();
});

//GET A SINGLE STATE
app.get("/api/summary/:id", (req, res, next) => {
    api.getSingleState(req, res);
});

//CREATE NEW STATE
app.post("/api/summary/", (req, res, next) => {
   api.createNewState(req, res);
})

//UPDATE A STATE
app.patch("/api/summary/:id", (req, res, next) => {
   api.updateState(req, res);
})

//DELETE A STATE
app.delete("/api/summary/:id", (req, res, next) => {
   api.deleteState(req, res);
})

// Default response for any other request
app.use(function(req, res){
    res.status(404);
});