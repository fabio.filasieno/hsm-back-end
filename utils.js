let db = require("./database.js");

function execSql(sql, params){
    return new Promise(function(resolve, reject) {
        db.run(sql, params, function (err, result) {
            if (err) {
                reject(new Error(err));
            } else {
                console.log("SQL SUCCESS!")
                resolve(true);
            }
        });
    })
}
 function getSql(sql, params){
    return new Promise(function(resolve, reject) {
        db.get(sql, params, (err, row) => {
            if (err) {
                console.log(err.message);
                reject(new Error(err));
            }
            else resolve(row);
        });
    })
}

exports.execSql = execSql;
exports.getSql = getSql;